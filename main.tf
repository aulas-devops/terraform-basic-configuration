terraform {
  required_version = "1.1.4"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.15.1"
    }
  }

}

provider "aws" {
  region  = "us-east-1"
  profile = "tf114"
}

resource "aws_s3_bucket" "fm-my-tf-test-bucket-01" {
  bucket = "fm-my-tf-test-bucket-01"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_acl" "example" {
  bucket = aws_s3_bucket.fm-my-tf-test-bucket-01.id
  acl    = "private"
}